
![Repositorio](https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/img/repositorio.png)
# Título del proyecto:

#### Repositorio académico de archivos JSON  

#### 💡Características:
Sitio web con un repositorio  de archivos json generados a partir de modelos de clases realizados en Java. Los datos son todos creados dinámicamente y forma anonimizada utilizando el componente gson. 

#### 🛠️Tecnologías

  - HTML5
  - Boostrap
  - Javascript

  ### 🔍 Demo
  Para ver el sitio web: [Repositorio JSON](https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/).

  ***
### 🧑  Autor(es)
Proyecto desarrollado por [Marco Adarme] (<madarme@ufps.edu.co>).


***
### 🏫Institución Académica   
Proyecto desarrollado en la Materia programación web del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Marco Adarme]: <http://madarme.co>
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
   
### 📆 Fecha inicial de registro: 
10/05/2021
